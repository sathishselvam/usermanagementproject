User Details Management Application For Active Ai - Task




This is a simple Spring boot Web application with front-end done using HTML,CSS and jQuery  and the back-end is done using 
Spring boot , JPA , Hibernate ORM which has the following flow : 

    1. Login page :
          
          User enters UserId and Password and then has to click login button.
          
    
    2.If user's role is admin then User Details page will open or if he is a user who has any role other than admin then 
      Greetings page will load.
    
    3.In user Details page : 
         
         a.It displays all available users and their details.
         
         b.Add user button will make any user who is an admin to create a new user.
         
         c.Edit button will edit details of existing user.
         
         d.Delete button will delete user.
    
    Known defects :
        
        1.Session maintainance is not done.
        
        2.Url security is not done.
        
        3.Form element specific validation is not done.
        
    
    Configurations : 
    
        Edit the "application.properties" file under resources to configure accordingly.
    
        1.Tomcat server port :
        
            server.port = <PORT_NUMBER> (PORT_NUMBER) on which tomcat should run
            
            example : server.port = 8082
        
        2.Database configurations for mysql
        
            
            spring.datasource.url=jdbc:mysql://localhost:<PORT_NUMBER>/<DATABASE_NAME>
            spring.datasource.username=<USER_NAME>
            spring.datasource.password=<PASS_WORD>
            spring.datasource.driver-class-name=com.mysql.jdbc.Driver
            
    
    Steps to be performed before running the project in local : 
    
        Start your mysql and create a database with name as same as in application.properties file.Then do the following.
        
        1.Create two tables in database : 
            
            a.Table name : "logindetails"
            
              Columns : 
                
                i.id - varchar
                
                ii.password - varchar
                
                iii.role - varchar
                
            b.Table name : "userdetails"
            
              Columns : 
                
                i.id - varchar
                
                ii.username - varchar
                
                iii.password - varchar
                
                iv.emailid - varchar
                
                v.role - varchar
                
                vi.phonenumber - varchar
                
    Step to run the project in local : 
        
        1.Open the project in eclipse.
        
        2.Right click " UserManagementProjectApplication.java" file and click "Run as java application".
        
        3.Open any of your browser and go to 
          "http://localhost:<PORT_NUMBER (as in application.properties)>/rest/users/index".
        
        Note : If any of the functionality not working properly , open console in check for error named "blocked because of 
               CORS policy". If so download "Allow CORS: Access-Control-Allow-Origin" add-on for your browser and on it.

    